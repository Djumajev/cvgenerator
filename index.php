<!DOCTYPE html>
<html>
    <head>
        <title>CV GENERATOR</title>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <style>
            body{
                background-color: #EEE;
                color: #282828;
                font-size: 110%;
            }

            center{
                display: block;
                width: 80%;
                text-align: left;
                margin: auto;
                margin-top: 2%;
            }

            txt{
                display: block;
                width: 100%;
                padding-bottom: 1%;
            }

            hr{
                height: 6px;
                background: url(http://ibrahimjabbari.com/english/images/hr-11.png) repeat-x 0 0;
                border: 0;
            }
        </style>
    </head>

    <body>
        <?php
            //Error function
            function error($str){
                if(isset($_POST['gen'])){
                    echo "<label style='color: red; font-size: 200%;'><br>".$str."-Nav norādīts</label>";
                    exit;
                } 
            }

            //Get image
            $image = "";

             if(isset($_FILES["filename"]["tmp_name"]))
                if(is_uploaded_file($_FILES["filename"]["tmp_name"])) {
                    move_uploaded_file($_FILES["filename"]["tmp_name"], "./uploaded/".$_FILES["filename"]["name"]);
                    $image = "./uploaded/".$_FILES["filename"]["name"];
                }

            //Get name
            $name = "";
            if(isset($_POST['name']) && $_POST['name'] != "")
                $name = $_POST['name'];
            else error("Vārds");

            //Get lastname
            $lastname = "";
            if(isset($_POST['lastname']) && $_POST['lastname'] != "")
                $lastname = $_POST['lastname'];
            else error("Uzvārds");

            //Get birthday
            $birthday = "";
            if(isset($_POST['date']) && $_POST['date'] != "")
                $birthday = $_POST['date'];
            else error("Dzimšanas datums");

            //Get email
            $email = "";
            if(isset($_POST['email']) && $_POST['email'] != "")
                $email = $_POST['email'];
            else error("Email");

            //Get all schools
            $schools = array();
            $schoolDateStart = array();
            $schoolDateEnd = array();
            $special = array();
            if(isset($_POST['school']))
                $schools = $_POST['school'];
            if(isset($_POST['schoolDateStart']) )
                $schoolDateStart = $_POST['schoolDateStart'];
            if(isset($_POST['schoolDateEnd']) )
                $schoolDateEnd = $_POST['schoolDateEnd'];
            if(isset($_POST['special']) )
                $special = $_POST['special'];


            //Get all leanguages
            $leanguage = array("Latviešu", "Krievu", "Angļu");
            $speak = array();
            $read = array();
            $write = array();

            if(isset($_POST['leang']))
                for($i = 0; $i < count($_POST['leang']); $i++)
                    array_push($leanguage, $_POST['leang'][$i]);

            if(isset($_POST['speak'])){
                $speak = $_POST['speak'];
                for($i = 0; $i < count($_POST['speak']); $i++)
                    if($speak[$i] == "")
                        error($leanguage[$i]." runātprasme");
            }

            if(isset($_POST['read'])){
                $read = $_POST['read'];
                for($i = 0; $i < count($_POST['read']); $i++)
                    if($read[$i] == "")
                        error($leanguage[$i]." runātprasme");
            }

            if(isset($_POST['speak'])){
                $speak = $_POST['speak'];
                for($i = 0; $i < count($_POST['speak']); $i++)
                    if($speak[$i] == "")
                        error($leanguage[$i]." lasītprasme");
            }

            if(isset($_POST['write'])){
                $write = $_POST['write'];
                for($i = 0; $i < count($_POST['write']); $i++)
                    if($write[$i] == "")
                        error($leanguage[$i]." rakstītprasme");
            }

            //Generate PDF
            if(isset($_POST['gen'])){
               require_once('./fpdf181/fpdf.php');

                class PDF extends FPDF{
                    function DrawHeader() {
                        $this->Image('logo.png', 10, 6, 30);
                        $this->SetFont('Times', 'B', 30);
                        $this->Cell(80);
                        $this->Cell(30, 10, 'CV', 0, 1);
                        $this->Ln(20);
                    }

                    function ChapterTitle($label){
                        $this->SetFont('Times', '', 20);
                        $label = iconv('UTF-8', 'windows-1257', $label);
                        $this->Cell(0, 6, $label, 0, 1);
                        $this->SetFillColor(180, 180, 200);
                        $this->Ln(1);
                        $this->Cell(0, 1, "", 0, 1, 'L', true);
                        $this->Ln(6);
                    }

                    function Data($msg1, $msg2, $isBold = false, $isMail = false, $difference = 0){
                        $this->SetFont('Times', '', 15);
                        $this->Cell(20 + $difference);
                        $this->Cell(35, 0, $msg1, 0, 1, 'R');
                        $this->Cell(53 + $difference);
                        if($isBold) $this->SetFont('Times', 'B', 15);
                        elseif($isMail) $this->SetTextColor(0, 0, 255);
                        $this->Cell(60, 0, $msg2, 0, 1);

                        if($isBold) $this->SetFont('Times', '', 15);
                        elseif($isMail) $this->SetTextColor(0, 0, 0);
                        $this->Ln(10);
                    }

                    function SubHeader($msg){
                        $this->SetFont('Times', 'B', 15);
                        $this->Cell(0, 0, $msg, 0, 1, 'L');
                        $this->SetFont('Times', '', 15);
                        $this->Ln(10);
                    }

                    function LeanguagesTable($header, $data){
                        $this->SetFillColor(220, 220, 250);
                        $this->setDrawColor(220, 220, 250);
                        // Header
                        $i = 0;
                        foreach($header as $col){
                            if($i != 0) $this->Cell(45, 7, $col, 1, 0, 'L', 1);
                            else $this->Cell(10, 7, $col, 1, 0, 'L', 1);
                            $i++;
                        }
                        $this->Ln();
                        // Data
                        foreach($data as $row)
                        {
                            $i = 0;
                            foreach($row as $col){
                                if($i != 0) $this->Cell(45, 6, $col, 1);
                                else $this->Cell(10, 6, $col, 1);
                                $i++;
                            }
                            $this->Ln();
                        }
                        $this->Ln(10);
                    }
                }
                
                $pdf = new PDF();
                $pdf->AddFont('Times','','times.php');
                $pdf->AliasNbPages();
                $pdf->AddPage();

                //header
                $pdf->DrawHeader();

                $pdf->ChapterTitle('Pamatinformācija ');
                //Image
                if($image != "") $pdf->Image($image, 50, 50, 100, 50);
                $pdf->Ln(60);
        
                //Vards Uzvards
                $msg = iconv('UTF-8', 'windows-1257', "Vārds, Uzvārds: ");
                $msg2 = iconv('UTF-8', 'windows-1257', $name.", ".$lastname);
                $pdf->Data($msg, $msg2, true);

                //Dzimsana
                $msg = iconv('UTF-8', 'windows-1257', "Dzimšanas datums: ");
                $msg2 = iconv('UTF-8', 'windows-1257', $birthday);
                $pdf->Data($msg, $msg2);

                //E-mail
                $msg = iconv('UTF-8', 'windows-1257', "E-pasta adrese: ");
                $msg2 = iconv('UTF-8', 'windows-1257', $email);
                $pdf->Data($msg, $msg2, false, true);

                //Valodas
                $pdf->ChapterTitle('Iemaņas un zināšanas');
                $msg = iconv('UTF-8', 'windows-1257', "Valodu zināšanas: ");
                $pdf->SubHeader($msg);
                $header = array(
                    iconv('UTF-8', 'windows-1257', " "),
                    iconv('UTF-8', 'windows-1257', "Valoda"),
                    iconv('UTF-8', 'windows-1257', "Runātprasme"),
                    iconv('UTF-8', 'windows-1257', "Lasītprasme"),
                    iconv('UTF-8', 'windows-1257', "Rakstītprasme")
                );
                $data = array();
                for($i = 0; $i < count($leanguage); $i++){
                    $row = array(
                        iconv('UTF-8', 'windows-1257', $i."."),
                        iconv('UTF-8', 'windows-1257', $leanguage[$i]),
                        iconv('UTF-8', 'windows-1257', $speak[$i]),
                        iconv('UTF-8', 'windows-1257', $read[$i]),
                        iconv('UTF-8', 'windows-1257', $write[$i])
                    );
                    array_push($data, $row);
                }
                $pdf->LeanguagesTable($header, $data);

                //Izglitiba
                $pdf->ChapterTitle('Izglītība');
                for($i = 0; $i < count($schools); $i++){
                    $msg = iconv('UTF-8', 'windows-1257', "Izglītības iestādes nosaukums: ");
                    $msg2 = iconv('UTF-8', 'windows-1257', $schools[$i]);
                    $pdf->Data($msg, $msg2, true, false, 10);

                    $msg = iconv('UTF-8', 'windows-1257', "Gads no: ");
                    $msg2 = iconv('UTF-8', 'windows-1257', $schoolDateStart[$i]);
                    $pdf->Data($msg, $msg2, false, false, 10);

                    $msg = iconv('UTF-8', 'windows-1257', "Gads līdz: ");
                    $msg2 = iconv('UTF-8', 'windows-1257', $schoolDateEnd[$i]);
                    $pdf->Data($msg, $msg2, false, false, 10);

                    $msg = iconv('UTF-8', 'windows-1257', "Specialitāte: ");
                    $msg2 = iconv('UTF-8', 'windows-1257', $special[$i]);
                    $pdf->Data($msg, $msg2, false, false, 10);
                }

                //Rakstam
                $pdf->Output();

                $last_id = 0;
                //Connect to DB and load
                $servername = "localhost";
                $username = "root";
                $password = "";
                $conn = new mysqli($servername, $username, $password, 'CVGenerator');

                $sql = "INSERT INTO persone VALUES (NULL, '$name', '$lastname', '$birthday', '$email', '$image');";
                if($conn->query($sql) === TRUE)
                    $last_id = $conn->insert_id;
                $fileName = "./CV/".$last_id.".pdf";

                for($i = 0; $i < count($leanguage); $i++)
                    $conn->query("INSERT INTO leanguage VALUES (NULL, '$last_id', '$leanguage[$i]', '$write[$i]', '$read[$i]', '$speak[$i]');");

                $conn->query("INSERT INTO file VALUES (NULL, '$last_id', '$fileName');");
                
                for($i = 0; $i < count($school); $i++)
                    $conn->query("INSERT INTO school VALUES (NULL, '$last_id', '$school[$i]', '$schoolDateStart[$i]', '$schoolDateEnd[$i]', '$special[$i]');");

                $conn->close();


                $pdf->Output($fileName, 'F');
            }

        ?>

        <center>
            <form method=POST enctype="multipart/form-data">
                <txt>Vārds: <input type=text name=name placeholder="Vārds"></txt> <hr>
                <txt>Uzvārds: <input type=text name=lastname placeholder="Uzvārds"></txt><hr>
                <txt>Dzimšanas datums: <input type=date name=date></txt><hr>
                <txt>E-pasts: <input type=email name=email placeholder="example@e-past.com"></txt><hr>

                <txt>Izglītības iestādes: <button class="w3-btn w3-blue" type=button onclick="addSchool()">Pievienot</button></txt>
                <schools id=sch></schools><hr>

                <txt>Valodu zināšanas: <button class="w3-btn w3-blue" type=button onclick="addLeanguage()">Pievienot</button></txt>
                <leanguges id=len>
                    <div style='border: 1px solid black;'>
                        <txt><label style='font-weight: bold;'>Latviešu</label><br>
                            Runātprasme: <input type=text name=speak[] placeholder='Runātprasme'>
                            Lasītprasme: <input type=text name=read[] placeholder='Lasītprasme'>
                            Rakstītprasme: <input type=text name=write[] placeholder='Rakstītprasme'>
                        </txt>
                    </div>

                    <div style='border: 1px solid black;'>
                        <txt><label style='font-weight: bold;'>Krievu</label><br>
                            Runātprasme: <input type=text name=speak[] placeholder='Runātprasme'>
                            Lasītprasme: <input type=text name=read[] placeholder='Lasītprasme'>
                            Rakstītprasme: <input type=text name=write[] placeholder='Rakstītprasme'>
                        </txt>
                    </div>

                    <div style='border: 1px solid black;'>
                        <txt><label style='font-weight: bold;'>Angļu</label><br>
                            Runātprasme: <input type=text name=speak[] placeholder='Runātprasme'>
                            Lasītprasme: <input type=text name=read[] placeholder='Lasītprasme'>
                            Rakstītprasme: <input type=text name=write[] placeholder='Rakstītprasme'>
                        </txt>
                    </div>
                </leanguges><hr>

                <txt>Bilde: <input class="w3-btn w3-blue" type="file" name="filename"></txt><hr>
                <txt><input class="w3-btn w3-blue" type=submit name=gen value="Ģenerēt PDF"></txt><hr>
            </form>
        </center>

        <script>
            var schools = document.getElementById("sch");
            function addSchool(){
                let string = "<div style='border: 1px solid black;'>" +
                                "<txt>Iestādes nosaukums: <input type=text name=school[] placeholder='Iestādes nosaukums'></txt>" +
                                "<txt>Gads no <input type=date name=schoolDateStart[]> - līdz <input type=date name=schoolDateEnd[]></txt>" +
                                "<txt>Specialitāte: <input type=text name=special[] placeholder='Specialitāte'></txt>" +
                                "</div>";

                schools.innerHTML += string;
            }

            var leanguages = document.getElementById("len");
            function addLeanguage(){
                let string = "<div style='border: 1px solid black;'>" +
                                "<txt><label style='font-weight: bold;'><input type=text name=leang[] placeholder='Valoda'></label><br>" +
                                 "Runātprasme: <input type=text name=speak[] placeholder='Runātprasme'>" +
                                 "Lasītprasme: <input type=text name=read[] placeholder='Lasītprasme'>" +
                                 "Rakstītprasme: <input type=text name=write[] placeholder='Rakstītprasme'>" +
                                "</txt>" +                            
                            "</div>";
                leanguages.innerHTML += string;
            }

        </script>

    </body>
</html>